package com.daniel.r2dbc;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.r2dbc.DataR2dbcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.r2dbc.core.DatabaseClient;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@ComponentScan(basePackages = "com.daniel.r2dbc")
@DataR2dbcTest
public class PlayerRepositoryTests {

    private Logger logger = LoggerFactory.getLogger(PlayerRepositoryTests.class);

    @Autowired
    private DatabaseClient databaseClient;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private PlayerRepository playerRepository;

    @Test
    public void testCreatePlayer() {
        Player player = new Player("Elliot", 38);
        playerRepository.save(player).then().as(StepVerifier::create).verifyComplete();

        Mono<Void> playerMono = playerService.createPlayer("Max", 12);

        playerMono.as(StepVerifier::create)
                .verifyComplete();

        playerRepository.findByName("Max")
                .as(StepVerifier::create)
                .assertNext(maxPlayer -> {
                    assertThat(maxPlayer).satisfies(
                            player1 -> {
                                assertThat(maxPlayer.getName()).isEqualTo("Max");
                                assertThat(maxPlayer.getAge()).isEqualTo(12);
                            });
                }).verifyComplete();

        playerMono = playerService.createPlayer("Max", 12);
        playerMono.as(StepVerifier::create)
                .verifyComplete();

        Flux<Player> maxPlayerFlux = playerRepository.findAll();
        maxPlayerFlux.subscribe(player1 -> {
            logger.info("{}", player1);
        });
    }
}
