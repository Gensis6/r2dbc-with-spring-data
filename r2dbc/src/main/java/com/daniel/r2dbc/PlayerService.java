package com.daniel.r2dbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class PlayerService {

    @Autowired
    private PlayerRepository playerRepository;

    public Mono<Void> createPlayer(String name, int age) {
        return playerRepository.findByName(name)
                .switchIfEmpty(Mono.just(new Player(name, age))
                        .flatMap(playerRepository::save))
                .then();
    }
}
